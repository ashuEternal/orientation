**GitLab**

- GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager.
-   It provides wiki, issue-tracking and continuous integration/continuous deployment pipeline features, using an open-source license, developed by GitLab Inc.
- It follows an open-core development model where the core functionality is released under an open-source (MIT) license while the additional functionality is under a proprietary license. 
![GitLab](https://gitlab.com/ashuEternal/orientation/-/blob/master/extras/final.png)

**GIT**

*Git is version-control software which lets you easily keep track of every revision you and your team make during the development of your software.*

**REQUIRED VOCABULARY**

- ***Repository*** : Collection of file and folders that you are using git to track.
- ***Commit*** : Commit is like making a checkpoint(savepoint) for the current progress of work.
- ***Push*** : It is syncing your commits to GitLab or GitHub.
- ***Branch*** : These are separate instances of the code that is different from the main codebase. It can be thought of as a branch originating from the source tree.
- ***Merge*** : Merging is just what it sounds like: integrating two branches together.
- ***Clone*** : It means making a copy of entire online repository on your local machine.
- ***Fork*** : Forking is a lot like cloning, only instead of making a duplicate of an existing repo on your local machine, you get an entirely new repo of that code under your own name.

**INSTALLING GIT**
- ***For LINUX*** : On terminal, type : `sudo -apt install git`
- ***For Windows*** : Download the installer and run it.

**THREE STATES OF GIT**

*Files in a repository go through three stages before being under version control with git:*
- ***Committed*** means that the data is safely stored in your local database.
- ***Modified*** means that you have changed the file but have not committed it to your database yet.
- ***Staged*** means that you have marked a modified file in its current version to go into your next commit snapshot.

*At one time there are 3/4 different trees of your software code/repository are present.*
![Different_trees_in_repository](https://gitlab.com/iotiotdotin/project-internship/orientation/-/wikis/extras/Git.png)

**Git Workflow**

1. ***Clone the repo*** `git clone <link-to-repository> `
2. ***Make a new branch*** `git checkout -b <your-branch-name>`
3. ***Modify files in working tree***
4. ***Add the modified files*** `git add .`
5. ***Commit the files into your local repository*** `git commit -m <commit-message>`
6. ***Push it back to remote repository*** `git push origin <branch-name>`
